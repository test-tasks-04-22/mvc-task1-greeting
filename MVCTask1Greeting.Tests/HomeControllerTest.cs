using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using MVCTask1Greeting.Controllers;

namespace MVCTask1Greeting.Tests;

public class HomeControllerTests
{
    [Test]
    public void ViewNotNull()
    {
        HomeController homeController = new HomeController();
        ViewResult result = homeController.Index() as ViewResult;
        Assert.NotNull(result);
    }

    [Test]
    public void ViewName()
    {
        HomeController homeController = new HomeController();
        ViewResult result = homeController.Index() as ViewResult;
        Assert.AreEqual("Index", result?.ViewName);
    }
}