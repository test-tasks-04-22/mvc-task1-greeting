using Microsoft.AspNetCore.Mvc;

namespace MVCTask1Greeting.Controllers;

public class HomeController : Controller
{
    public IActionResult Index()
    {
        int hour = DateTime.Now.Hour;
        switch (hour)
        {
            case < 6:
                ViewBag.Hello = "Доброй ночи,";
                break;
            case < 12:
                ViewBag.Hello = "Доброе утро,";
                break;
            case >= 18:
                ViewBag.Hello = "Добрый вечер,";
                break;
            default:
                ViewBag.Hello = "Добрый день,";
                break;
        }
        return View("Index");
    }
}